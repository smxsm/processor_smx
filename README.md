Processor
=========

Turbo-Charge your OXID-eSales CE/PE
-----------------------------------

Processor minifies, compresses and hashes any HTML files, .css and .js files within your OXID-Installation. All resources use 'forever-caching' or 'fingerprinting' to prevent unnecessary 304 requests (see [https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content](https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content)). This speeds up your page quite dramatically.

Everything is automatic. Install into /modules/processor. Activate. Clear your /tmp/ cache. Done. 

*Note:* don't use Processor in a Shop that is installed in a sub-folder. There is a small chance that the internet _might_ explode.

*Note 2:* Processor may not work if you use Subshops. Please be careful if you use EE!


LESS-Parser
-----------

.less files automatically get compiled server-side, if you like.


Screenshots
-----------

![Before and after shot](https://bitbucket.org/gn2netwerk/processor/raw/info/before_after.png)


A little bit more speed
-----------------------

If you want to go a little further, add these additional rules to your .htaccess:


    ######## GN2 ###############################
    # Additional processor rules
    <IfModule mod_headers.c>
        Header unset ETag
    </IfModule>

    FileETag None

    # compress javascript, css:
    AddOutputFilterByType DEFLATE text/plain
    AddOutputFilterByType DEFLATE text/html
    AddOutputFilterByType DEFLATE image/gif
    AddOutputFilterByType DEFLATE image/png
    AddOutputFilterByType DEFLATE image/jpg
    AddOutputFilterByType DEFLATE image/jpeg
    AddOutputFilterByType DEFLATE image/pjpeg
    AddOutputFilterByType DEFLATE text/xml
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE text/javascript
    AddOutputFilterByType DEFLATE application/rss+xml
    AddOutputFilterByType DEFLATE application/javascript
    AddOutputFilterByType DEFLATE application/x-javascript

    <FilesMatch "\\.(js|css|html|htm|php|xml)$">
      SetOutputFilter DEFLATE
    </FilesMatch>

    <IfModule mod_expires.c>
        ExpiresActive on
        ExpiresDefault                                      "access plus 1 month"
        ExpiresByType text/css                              "access plus 1 year"
        ExpiresByType application/json                      "access plus 0 seconds"
        ExpiresByType application/xml                       "access plus 0 seconds"
        ExpiresByType text/xml                              "access plus 0 seconds"
        ExpiresByType image/x-icon                          "access plus 1 week"
        ExpiresByType text/x-component                      "access plus 1 month"
        ExpiresByType text/html                             "access plus 0 seconds"
        ExpiresByType application/javascript                "access plus 1 year"
        ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
        ExpiresByType text/cache-manifest                   "access plus 0 seconds"
        ExpiresByType audio/ogg                             "access plus 1 month"
        ExpiresByType image/gif                             "access plus 1 month"
        ExpiresByType image/jpeg                            "access plus 1 month"
        ExpiresByType image/png                             "access plus 1 month"
        ExpiresByType video/mp4                             "access plus 1 month"
        ExpiresByType video/ogg                             "access plus 1 month"
        ExpiresByType video/webm                            "access plus 1 month"
        ExpiresByType application/atom+xml                  "access plus 1 hour"
        ExpiresByType application/rss+xml                   "access plus 1 hour"
        ExpiresByType application/font-woff                 "access plus 1 month"
        ExpiresByType application/vnd.ms-fontobject         "access plus 1 month"
        ExpiresByType application/x-font-ttf                "access plus 1 month"
        ExpiresByType font/opentype                         "access plus 1 month"
        ExpiresByType image/svg+xml                         "access plus 1 month"
    </IfModule>
    ######## /GN2 ###############################


REDAXO
------

Processor also works with REDAXO, just call the ``<?php echo Processor::parse($filename); ?>`` function to get a minified URL.
