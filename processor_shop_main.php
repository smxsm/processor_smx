<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */

/**
 * Deletes tmp contents whenever shop mode is changed
 */
class processor_shop_main extends processor_shop_main_parent {
    
    /**
     * Saves changed main shop configuration parameters.
     *
     * @return null
     */
    public function save()
    {

        $myConfig = $this->getConfig();
        $soxId = $this->getEditObjectId();
        $oDb = oxDb::getDb();

        $aParams  = oxConfig::getParameter( "editval");
        // checkbox handling
        $aParams['oxshops__oxactive'] = ( isset( $aParams['oxshops__oxactive'] ) && $aParams['oxshops__oxactive'] == true )? 1 : 0;
        $aParams['oxshops__oxproductive'] = ( isset( $aParams['oxshops__oxproductive']) && $aParams['oxshops__oxproductive'] == true) ? 1 : 0;

        $bIsProductive = $oDb->getOne("select oxproductive from oxshops where oxid = " . $oDb->quote($soxId));
        // if shop active and productive flag changed, clear tmp dir
        if($aParams['oxshops__oxactive'] && ($bIsProductive != $aParams['oxshops__oxproductive'])) {
            processor_setup::clearTmp();
        }
        
        parent::save();
    }
    

    
}
