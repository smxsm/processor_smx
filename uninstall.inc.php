<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author (contributing) jdlx <honeypot@rexdev.de>
 */

$REX['ADDON']['install']['processor'] = 0;
