<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */

/**
 * De-/Activation module for processor
 * Clears tmp contents every time the module is de-/activated
 * to make sure the users don't get a white page when browsing the shop.
 */
class processor_setup extends oxSuperCfg {

    /**
     * Setup routine
     */
    public static function onActivate() {
        self::clearTmp();
    }    
    /**
     * Removal routine
     */
    public static function onDeactivate() {
        self::clearTmp();
    }    
    
    /**
     * Empty tmp dir
     */
    public static function clearTmp() {
        $oConf   = oxRegistry::getConfig();
        $sTmpDir = realpath($oConf->getShopConfVar('sCompileDir'));
        $aFiles = glob($sTmpDir.'/*{.php,.txt}',GLOB_BRACE);
        $aFiles = array_merge($aFiles, glob($sTmpDir.'/smarty/*.php'));
        $aFiles = array_merge($aFiles, glob($sTmpDir.'/ocb_cache/*.json'));
        if(count($aFiles) > 0)
        {
            foreach($aFiles as $file) {
                @unlink($file);
            }
        }
    }
}

?>
