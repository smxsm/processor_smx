<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */

class Processor_OxShopControl extends Processor_OxShopControl_parent
{
    /**
     * render oxView object
     *
     * @param oxView $oViewObject view object to render
     *
     * @return string
     */
    protected function _render($oViewObject)
    {
        $sOutput = parent::_render($oViewObject);
        
        // only minify in productive mode
        $oConfig = oxRegistry::getConfig();
        if(isAdmin() || !$oConfig->isProductiveMode()) {
            return $sOutput;
        }
        
        // compress complete HTML code?
        if($oConfig->getConfigParam('gn2_minifyhtml')) {
            $sOutput = $this->_trim_whitespace($sOutput);
        }
        return $sOutput;
    }
    
    /**
     * Function to trim any whitespace with some exceptions, e.g. inside script tags
     * Removes multi spaces, tabs, newlines, ...
     * @see http://stackoverflow.com/questions/5312349/minifying-final-html-output-using-regular-expressions-with-codeigniter
     * @param string $text The text to trim
     * @return string The trimmed text
     */
    protected function _trim_whitespace($text) // 
    {
        $re = '%# Collapse whitespace everywhere but in blacklisted elements.
            (?>             # Match all whitespans other than single space.
              [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
            | \s{2,}        # or two or more consecutive-any-whitespace.
            |<!--(?!\[if).*?-->+ # or comments which are not IE conditional comments (smx_sm)
            |( class| style)=[\'"][\'"]  # empty class or style attributes (smx_sm)
            ) # Note: The remaining regex consumes no text at all...
            (?=             # Ensure we are not in a blacklist tag.
              [^<]*+        # Either zero or more non-"<" {normal*}
              (?:           # Begin {(special normal*)*} construct
                <           # or a < starting a non-blacklist tag.
                (?!/?(?:textarea|pre|script)\b)
                [^<]*+      # more non-"<" {normal*}
              )*+           # Finish "unrolling-the-loop"
              (?:           # Begin alternation group.
                <           # Either a blacklist start tag.
                (?>textarea|pre|script)\b
              | \z          # or end of file.
              )             # End alternation group.
            )  # If we made it here, we are not in a blacklist tag.
            %Six';
        return preg_replace($re, " ", $text);
    }
}