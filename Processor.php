<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author (contributing) jdlx <honeypot@rexdev.de>
 */



class Processor
{
    public static $extensions       = array('css', 'less', 'js');
    public static $minify_js        = true;
    public static $minify_css       = true;
    public static $compile_less     = true;
    public static $debug            = false; # switch err display & force regenerate


    static public function parse($sourcepath, $dev = false)
    {
        global $REX;

        if (!file_exists($sourcepath)) {
            return $sourcepath.'.processor-404-error';
        }

        # if dev context: don't minify, use debug ..
        if ($dev == true) {
            self::$minify_js  = false;
            self::$minify_css = false;
            self::$compile_less = false;
            self::$debug      = true;
        }

        if (self::$debug) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }

        # handle absolute URLs
        $url = parse_url($sourcepath);
        if (isset($url['scheme'])) {
            # self::handleRemotes($sourcepath); (add local caching for externals?)
            return $sourcepath;
        }

        # Get file extension
        $ext = pathinfo($sourcepath, PATHINFO_EXTENSION);

        # Skip if wrong filetype
        if(!in_array($ext, self::$extensions)) {
            return $sourcepath;
        }

        # Sanitize
        $sourcepath = urldecode($sourcepath);
        $sourcepath = str_replace(array("\\", "..", chr(0)), '', $sourcepath);
        $sourcepath = ltrim($sourcepath, '/');

        # Cachefile
        $sha1      = substr(sha1_file($sourcepath), 0, 8);
        $out_ext   = $ext == 'less' ? 'css' : $ext;
        $env_ext   = $dev == false ? '' : '.dev';
        $cachepath = str_replace('.'.$ext, '.p.'.$sha1.$env_ext.'.'.$out_ext, $sourcepath);

        # if processed file exists -> nothing to do..
        if(file_exists($cachepath)) {
            return $cachepath;
        }

        # Serve or Generate
        try {
            switch ($ext) {
                case 'less':
                    if(self::$compile_less) {
                        if (!file_exists($cachepath) || self::$debug) {
                            self::clearCache(str_replace($sha1, '*', $cachepath));

                            $less = new lessc;
                            $less->setPreserveComments(false);
                            $less->checkedCompile($sourcepath, $cachepath);
                            if (self::$minify_css) {
                                $CssMin = new CssMin;
                                file_put_contents($cachepath, $CssMin->run(file_get_contents($cachepath)));
                            }
                        }
                    }
                    return $cachepath;
                    break;

                case 'css':
                    if (self::$minify_css) {
                        if((!file_exists($cachepath) || self::$debug)) {
                            self::clearCache(str_replace($sha1, '*', $cachepath));
                            $CssMin = new CssMin;
                            file_put_contents($cachepath, $CssMin->run(file_get_contents($sourcepath)));
                        }
                        return $cachepath;
                    } else {
                        return $sourcepath;
                    }
                    break;

                case 'js':
                    if (self::$minify_js) {
                        if((!file_exists($cachepath) || self::$debug)) {
                            self::clearCache(str_replace($sha1, '*', $cachepath));
                            file_put_contents($cachepath, JsMin::minify(file_get_contents($sourcepath)));
                        }
                        return $cachepath;
                    } else {
                        return $sourcepath;
                    }
                    break;
            }
        } catch (Exception $e) {
            file_put_contents($cachepath.'.processor-generate-error', var_export($e,true));
            return $cachepath.'.processor-generate-error';
        }

    }

    static public function clearCache($pattern)
    {
        foreach ( (array) glob($pattern) as $cacheFile) {
            unlink($cacheFile);
        }
    }

    public static function minifyJS($bool) {
        self::$minify_js = $bool;
    }


    public static function minifyCSS($bool) {
        self::$minify_css = $bool;
    }
    
    public static function compileLess($bool) {
        self::$compile_less = $bool;
    }

    public static function debug($bool) {
        self::$debug = $bool;
    }


}
