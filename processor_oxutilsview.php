<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */

class Processor_OxUtilsView extends Processor_OxUtilsView_parent
{
    public function getSmarty($blReload = false)
    {
        $oConfig = oxRegistry::getConfig();
        // only minify in productive mode
        if (isAdmin() || !$oConfig->isProductiveMode()) {
            return parent::getSmarty($blReload);
        }
        
        if (!class_exists('Processor')) {
            $p = dirname(__FILE__);
            require_once $p.'/Processor.php';
            if(!class_exists('lessc')) {
                require_once $p.'/lessc.php';
            }
            require_once $p.'/cssmin.php';
            require_once $p.'/jsmin.php';
        }

        $path = rtrim($oConfig->getConfigParam('sShopDir'), '/');
        $smarty = parent::getSmarty($blReload);
        $smarty->unregister_function('oxstyle');
        $smarty->register_function('oxstyle', array($this,'processor_smarty_function_oxstyle'));
        $smarty->unregister_function('oxscript');
        $smarty->register_function('oxscript', array($this,'processor_smarty_function_oxscript'));

        foreach (array('smarty_function_oxstyle', 'smarty_function_oxscript') as $func) {
            if (!function_exists($func)) {
                $file = $path.'/core/smarty/plugins/'.str_replace('smarty_function_', 'function.', $func).'.php';
                require_once $file;
            }
        }
        return $smarty;
    }

    function processor_smarty_function_oxstyle($params, &$smarty)
    {
        $output = smarty_function_oxstyle($params, $smarty);
        return $this->processor_parse($output);
    }

    function processor_smarty_function_oxscript($params, &$smarty)
    {
        $output = smarty_function_oxscript($params, $smarty);       
        return $this->processor_parse($output);
    }

    function processor_parse($output) {
        if (strlen($output) > 0) {
            $oConfig = oxConfig::getInstance();
            $noSSL = str_replace('/', '\/', $oConfig->getConfigParam('sShopURL'));
            $SSL = str_replace('/', '\/', $oConfig->getConfigParam('sSSLShopURL'));
            $pattern = "/(?P<attr>href|src)=\"(?P<domain>".$noSSL."|".$SSL.")(?P<path>.*?)?(\?(.*))\"/";
            $output = preg_replace_callback($pattern, array($this, 'processor_replace_callback'), $output);
        }
        return $output;
    }

    function processor_replace_callback($m)
    {
        $oConfig = oxRegistry::getConfig();
        $path = $m['path'];
        if (strpos($path, '.min.js') === false) {
            // set processor features
            Processor::compileLess($oConfig->getConfigParam('gn2_compileless'));
            Processor::minifyJS($oConfig->getConfigParam('gn2_minifyjs'));
            Processor::minifyCSS($oConfig->getConfigParam('gn2_minifycss'));
            // do the work :)
            $path = Processor::parse($m['path']);
        }
        $url = $m['attr'].'="'.$m['domain'].$path.'"';
        return $url;
    }

}