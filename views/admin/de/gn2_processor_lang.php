<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(

'charset'                                  => 'UTF-8',
    
'SHOP_MODULE_GROUP_GN2PRO'                 => "Processor Einstellungen",
'SHOP_MODULE_gn2_compileless'              => "LESS kompilieren",    
'SHOP_MODULE_gn2_minifyjs'                 => "Javascript minimieren",    
'SHOP_MODULE_gn2_minifycss'                => "CSS minimieren",    
'SHOP_MODULE_gn2_minifyhtml'               => "HTML minimieren",    

);

?>
