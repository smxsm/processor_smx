<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@shoptimax.de>
 * @author jdlx <honeypot@rexdev.de>
 */
$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(

'charset'                                  => 'UTF-8',
    
'SHOP_MODULE_GROUP_GN2PRO'                 => "Processor settings",
'SHOP_MODULE_gn2_compileless'              => "Compile LESS",    
'SHOP_MODULE_gn2_minifyjs'                 => "Minify Javascript",    
'SHOP_MODULE_gn2_minifycss'                => "Minify CSS",    
'SHOP_MODULE_gn2_minifyhtml'               => "Minify HTML",    

);

?>
