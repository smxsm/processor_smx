<?php
$sMetadataVersion = '1.1';

$aModule = array(
    'id'           => 'processor',
    'title'        => 'gn2 :: Processor',
    'description'  => 'Processor minifies, compresses and hashes any HTML files,
                       .css and .js files within your OXID-Installation. All
                       resources use \'forever-caching\' or \'fingerprinting\'
                       to prevent unnecessary 304 requests (see
                       <a style="text-decoration:underline;" href="https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content">
                       https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content</a>).
                       This speeds up your pages quite dramatically.',
    'thumbnail'    => 'processor.png',
    'version'      => '1.1',
    'author'       => 'gn2 netwerk',
    'url'          => 'http://www.gn2-netwerk.de',
    'email'        => 'kontakt@gn2-netwerk.de',
    'files'       => array(
        'processor_setup'   => 'processor/processor_setup.php',
    ),
    'extend'       => array(
        'oxutilsview'   => 'processor/processor_oxutilsview',
        'oxshopcontrol' => 'processor/processor_oxshopcontrol',
        'shop_main'     => 'processor/processor_shop_main'
    ),
    'events' => array(
        'onActivate' => 'processor_setup::onActivate',
        'onDeactivate' => 'processor_setup::onDeactivate'
    ),
    'settings' => array(
        array('group' => 'GN2PRO', 'name' => 'gn2_compileless', 'type' => 'bool',  'value' => '1'),
        array('group' => 'GN2PRO', 'name' => 'gn2_minifyjs', 'type' => 'bool',  'value' => '1'),
        array('group' => 'GN2PRO', 'name' => 'gn2_minifycss', 'type' => 'bool',  'value' => '1'),
        array('group' => 'GN2PRO', 'name' => 'gn2_minifyhtml', 'type' => 'bool',  'value' => '1'),
    ),
);